FROM debian:stable-slim
RUN apt-get update
RUN apt-get install -yqq \
	build-essential \
	autoconf \
	automake \
	libtool \
	m4 \
	autoconf-archive \
	pkg-config \
	lcov \
	python-pip \
	doxygen \
	check \
	git \
	curl \
	clang \
	valgrind \
	libssl-dev
RUN pip install sphinx sphinx-rtd-theme breathe
RUN rm -rf /var/lib/apt/lists/*
RUN apt-get clean
