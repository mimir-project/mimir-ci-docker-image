Mimir CI Docker Image
=====================
This custom docker image is used in the mimir CI jobs.
The image is build with GitLab CI and stored in
[mimir-ci-docker-image container registry](https://gitlab.com/mimir-project/mimir-ci-docker-image/container_registry)

It can be pulled with:
``docker pull registry.gitlab.com/mimir-project/mimir-ci-docker-image/master``

Rebuilding
----------
Weekly rebuild of the image.

Base Image
----------
[Debian stable-slim](https://hub.docker.com/r/library/debian/)

Packages
--------
* build-essential
* autoconf
* automake
* libtool
* m4
* autoconf-archive
* pkg-config
* lcov
* python-pip
* doxygen
* check
* git
* curl
* clang
* valgrind
* libssl-dev

Through `pip`
* sphinx
* sphinx-rtd-theme
* breathe
